import ConfigParser
import pypd
from pypd.errors import BadRequest
from time import sleep
import re
import traceback

class pagerduty:

    incident = None
    apikey = None
    routingkey = None
    phone = None
    room = None


    def __init__(self):
        Config = ConfigParser.ConfigParser()
        Config.read("/etc/bigredbutton/bigredbutton.cfg")
        self.apikey = Config.get('APIKeys','api')
        self.routingkey = Config.get('APIKeys','service')
        self.phone = Config.get('Phone','phone')
        self.room = Config.get('Phone','room')

    def trigger(self):
	try:
            pypd.api_key = self.apikey
            # API map here: https://v2.developer.pagerduty.com/docs/send-an-event-events-api-v2
            self.incident = pypd.EventV2.create(data={
                'routing_key': self.routingkey,
                'event_action': 'trigger',
                'payload': {
                    'summary': 'Someone pushed the Big Red Button. Please investigate.',
                    'severity': 'critical',
                    'source': 'The Big Red Button',
                    'custom_details': "Please call the conference room at: " + self.phone + " room#: " + self.room + ", or click the link in the ticket." 
                },
                'links': [{
                    'href': "tel:" + self.phone.replace('-','') + ',' + self.room.replace('-',''),
                    'text': "Click here to autodial conference line."
               }]
            })
            return self.incident['status']
        except BadRequest as b:
            print str(b)
            print str(self.incident)
            return b
        except Exception as e:
            print str(e)
            print str(self.incident)
            return e

    def status(self):
	try:
	    currentIncident = []
            timeout = 60 #shouldn't take more than 1m to get a status of a page
            while len(currentIncident) == 0 and timeout > 0:
              sleep(1)
              timeout = timeout - 1
              currentIncident = pypd.Incident.find(incident_key=self.incident['dedup_key'])
            if timeout < 1:
              raise Exception("Timeout trying to find incident.")

            return re.search('(?<=<Incident status=")\w+',str(currentIncident[0])).group(0)
            #gotta filter the result string

        except Exception as e:
            print str(self.incident)
            print str(self.incident['dedup_key'])
            print str(currentIncident)
            traceback.print_exc()
            return e

