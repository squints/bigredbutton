#!/usr/bin/python
import RPi.GPIO as GPIO
import pagerduty
import lcddriver
from time import sleep
import ConfigParser

# this is the cash money right here.
# get config, set the GPIO, init the lcd, start looping the activity code

Config = ConfigParser.ConfigParser()
Config.read("/etc/bigredbutton/bigredbutton.cfg")
buttonPin = int(Config.get("GPIO","button"))
GPIO.setmode(GPIO.BCM)
GPIO.setup(buttonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
lcd = lcddriver.lcd()
lcd.lcd_clear()
pd = pagerduty.pagerduty()

lcd.lcd_display_string("*The Big Red Button*",1)
lcd.lcd_display_string("                    ",2)
lcd.lcd_display_string("To start a firecall,",3)
lcd.lcd_display_string("  push the button   ",4)

while True:
    button = GPIO.input(buttonPin)
    if button == False: #weird that False = pressed, True = open
        lcd.lcd_clear()
        lcd.lcd_display_string("**BUTTON ACTIVATED!*",1)
        lcd.lcd_display_string("                    ",2)
        lcd.lcd_display_string("  Paging ProdSup... ",3)
        lcd.lcd_display_string("    Please Wait.    ",4)
        lcd.flash(4)
        pd.trigger()
        previousStatus = None
        while True:
            status = pd.status()
            if status == 'triggered':
                if previousStatus != 'triggered': #don't refresh the lcd if PD's in the same status
                    lcd.lcd_clear()
                    lcd.lcd_display_string("*FIRECALL TRIGGERED*",1)
                    lcd.lcd_display_string("~~~ProdSup Paged.~~~",2)
                    lcd.lcd_display_string("Please wait here for",3)
                    lcd.lcd_display_string("ProdSup to respond. ",4)
                    lcd.flash(4)
                sleep(5)
                previousStatus = 'triggered'
            elif status == 'acknowledged':
                if previousStatus != 'acknowledged':
                    lcd.lcd_clear()
                    lcd.lcd_display_string("*FIRECALL  RECEIVED*",1)
                    lcd.lcd_display_string(" Please call Conf.: ",2)
                    lcd.lcd_display_string("   " + Config.get('Phone','phone') + "   ",3)
                    lcd.lcd_display_string(" Room: " + Config.get('Phone','room') + "# ",4)
                    lcd.flash(4)
                sleep(5)
                previousStatus = 'acknowledged'
            elif status == 'resolved':
                lcd.lcd_clear()
                lcd.lcd_display_string("********************",1)
                lcd.lcd_display_string("*     Firecall     *",2)
                lcd.lcd_display_string("*     Resolved     *",3)
                lcd.lcd_display_string("********************",4)
                lcd.flash(4)
                sleep(15)
                lcd.lcd_display_string("*The Big Red Button*",1)
                lcd.lcd_display_string("                    ",2)
                lcd.lcd_display_string("To start a firecall,",3)
                lcd.lcd_display_string("  push the button   ",4)
                break
            else: # if the PD status isn't in the above three switches, then WTF?
                lcd.lcd_clear()
                lcd.lcd_display_string("**** OH NO! :( *****",1)
                lcd.lcd_display_string("Something went wrong",2)
                lcd.lcd_display_string("Email: bigredbutton@",3)
                lcd.lcd_display_string("   us: vpayusa.com  ",4)
                lcd.flash(16)
                sleep(15)
                lcd.lcd_display_string("*The Big Red Button*",1)
                lcd.lcd_display_string("                    ",2)
                lcd.lcd_display_string("To start a firecall,",3)
                lcd.lcd_display_string("  push the button   ",4)
                break

